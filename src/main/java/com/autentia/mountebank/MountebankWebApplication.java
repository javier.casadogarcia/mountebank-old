package com.autentia.mountebank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MountebankWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(MountebankWebApplication.class, args);
	}

}
